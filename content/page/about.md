---
title: About
subtitle: A possible new home for Will Murphy's blog
comments: false
---

Hello friends! I'm a little sick of wordpress, so I'm thinking about moving my
[real blog](https://willmurphyscode.net) over to GitLab pages. In the meantime,
I'm messing with GitLab to see whether I like it before I commit to the project.
